jQuery(function($){
  var prefix = 'api';
  $("body")
    .on("click", "#users_api_manager .addnew a", function(){
      var out = '';
      out +=  '<div class="form">' +
              '<label for="api_title">Webtitel</label><input name="title" id="api_title">' +
              '<label for="api_url">API-URL</label><input name="url" id="api_url">' +
              '<label for="api_cols">API-Spalten</label><textarea name="api_cols" id="api_cols"></textarea>' +
              '<button class="button-primary">Senden</button><span class="loading"></span>' +
              '</div>';
      $(this).parent().find(".form").remove();
      $(this).parent().append( out );
    })
    /*
    * Click API Edit
    */
    .on("click", "#users_api_manager .edit", function(){
      var out = '';
      $("#users_api_manager .addnew a").click();
      $.ajax({
          url: ajaxurl,
          data: {action: "usersImportByApi_api_single", class : "usersImportByApi", func : "api_single", post_id : $(this).parents('tr').attr("data-ID")},
          dataType: 'jsonp',
          type:'post',
          beforeSend: function(formData, jqForm, options) {},
          complete : function(jqXHR, textStatus) {
            var out = '',
                response = $.parseJSON( jqXHR.responseText );

            $("#users_api_manager .form #" + prefix + "_title").val( response.post_title );
            $("#users_api_manager .form #" + prefix + "_url").val( response.post_content );
            $("#users_api_manager .form #" + prefix + "_cols").val( response.api_cols );

            $("#users_api_manager .form .postID").remove();
            var update_input = '<div class="postID"><label for="api_post_id">PostID</label><input readonly name="post_id" id="api_post_id" value="'+ response.ID +'" ></div>';
            $("#users_api_manager .form").prepend( update_input );

            $("#users_api_manager .form .button-primary").text("Aktualisieren").after('<button class="delete button-secondary">Löschen</button>');
          },
      });
    })
    /*
    * Form button
    * Click API Save
    */
    .on("click", "#users_api_manager .form button.button-primary", function(){
      var selector = $(this).parent(),
          sel_loading = selector.find('.loading'),
          url = selector.find('#' + prefix + '_url').val(),
          title = selector.find('#' + prefix + '_title').val(),
          post_id = selector.find('#' + prefix + '_post_id').val();
          post_api_cols = selector.find('#' + prefix + '_cols').val();

      if( _.isEmpty(url) ){
        sel_loading.html('<p>Keine URL</p>');
        return;
      }
      if( !url.match(/^http([s]?):\/\/.*/) ){
        sel_loading.html('<p>Kein HTTP in URL</p>');
        return;
      }
      if(_.isEmpty(title)){
        sel_loading.html('<p>Kein Titel</p>');
        return;
      }

      $.ajax({
          url: ajaxurl,
          data: {action: "usersImportByApi_api_create", class : "usersImportByApi", func : "api_create", "api_post_id" : post_id, api_title : title, api_url : url, api_cols : post_api_cols},
          dataType: 'jsonp',
          type:'post',
          beforeSend: function(formData, jqForm, options) {
            selector.find('.loading').html( msloadingImage );
          },
          complete : function(jqXHR, textStatus) {
            var data = $.parseJSON( jqXHR.responseText );
            sel_loading.html('<p>Updated: '+data.post_title+'</p>');
            apiList();
          },
      });
    })
    /*
    * Click API Show button
    */
    .on("click", "#users_api_manager table .show", function(){
        var apisource = $(this).parents("tr").attr("data-source");
        $("#api_external_users .main").html( msloadingImage );
        getExtUsers($(this), apisource);
    })

    /*
    * Click Import button
    */
    .on("click", "#users_api_manager table .import", function(){

      var post_id   = $(this).parents("tr").attr("data-id");
      $(this).parents("tr").find(".show").click();

      setTimeout(function(){
        $("#api_external_users table .exists").remove();
        $("#api_external_users table tbody tr:first-child").click();
      },1000);
    })

    /*
    * SAVE External Users
    * Click External User Row
    */

    .on("click", "#api_external_users table tbody tr", function(){
      var data = {},
          row = $(this),
          table = row.parents("table"),
          rowData = JSON.parse(row.attr("data-ajax"));

      //Counter
      $("#api_external_users h2 span.progress").html( '/' + (table.find('tbody tr').length) );

      data =  {
        action         : "usersImportByApi_import",
        class          : "usersImportByApi",
        func           : "import",
        email          : rowData.email,
        confirm        : rowData.status,
        username       : ((rowData.username  != "null") ? rowData.username : ''),
        firstname      : ((rowData.firstname != "null") ? rowData.username : rowData.firstname),
        lastname       : ((rowData.lastname  != "null") ? rowData.lastname : ''),
        fullname       : ((rowData.firstname != "null") ? rowData.username : rowData.firstname + ' ' + rowData.lastname),
        herkunft       : rowData.source,
        herkunftsdatum : rowData.created
      };

      // console.log(rowData);
      $.ajax({
          url       : ajaxurl,
          data      : data,
          dataType  : 'jsonp',
          type      : 'post',
          beforeSend: function(formData, jqForm, options) {},
          complete  : function(jqXHR, textStatus) {

            var response = $.parseJSON( jqXHR.responseText );
            if( response.status ){
              row.next().click();
              row.remove();
            }
          }
      });
    });
    apiList();
    /*
    * fn API LIST
    */
    function apiList(){
      $.ajax({
          url: ajaxurl,
          data: {action: "usersImportByApi_apis", class : "usersImportByApi", func : "apis"},
          dataType: 'jsonp',
          type:'post',
          beforeSend: function(formData, jqForm, options) {},
          complete : function(jqXHR, textStatus) {

            var out = '',
                response = $.parseJSON( jqXHR.responseText );

            $.each( response, function(api_nr, api_value){
              out +=  '<tr data-id="'+ api_value.ID +'" data-apiurl="'+ api_value.post_content +'" data-apicols="'+api_value.api_cols+'" data-source="'+api_value.post_title+'">' +
                      '<td class="post_title">' + api_value.post_title + '</td>'+
                      '<td class="post_title">' + api_value.usercount + '</td>'+
                      '<td><button class="edit button-secondary">Edit</button></td>' +
                      '<td><button class="import button-secondary">Import</button></td>' +
                      '<td><button class="show button-secondary">Show</button></td>' +
                      '</tr>';
            });
            $("#users_api_manager table").html(out);
          },
      });
    }
    function getExtUsers(apiRow, source){
        $.post(ajaxurl,
          {
          action : "usersImportByApi_getExtUsers",
          class  : "usersImportByApi",
          func   : "getExtUsers",
          source : source
          },
          function(data){
            // console.log(data);
            showExtApiUsers(apiRow, data);
          }
        );
    }
    function showExtApiUsers(apiRow, getExtUsers){

      var post_id   = apiRow.parents("tr").attr("data-id"),
          apiurl    = apiRow.parents("tr").attr("data-apiurl"),
          apisource = apiRow.parents("tr").attr("data-source"),
          api_cols  = apiRow.parents("tr").attr("data-apicols").split(','),
          out       = '',
          rowclass  = '';


      $.getJSON(apiurl,function(data){

        //User count
        $("#api_external_users h2 span.usercount").remove();
        $("#api_external_users h2").append('<span class="usercount"> - Anzahl: '+data.length+'</span><span class="progress"></span>');

        //Save Usercount
        $.post(ajaxurl, {action: "usersImportByApi_api_update_usercount", class : "usersImportByApi", func : "api_update_usercount", "api_post_id" : post_id, usercount : data.length});


        out += '<table class="wp-list-table widefat striped" data-usercount="'+data.length+'">';
        out +=  '<thead><tr>';

        _.each(api_cols, function(col, ci){
          out +=  '<th>' + $.trim(col) + '</th>';
        });
        out +=  '<th>Source</th>';
        out +=  '</tr></thead>'+
                '<tbody>';

        _.each(data, function(val, idx){
          rowclass  = 'exists';
          if (_.where(getExtUsers, {"email" : val.email}).length < 1){
            rowclass = 'rownew';
          }
          //add Source
          val.source = apisource;

          out +=  '<tr class="'+rowclass+'" data-ajax=\''+JSON.stringify(val)+'\'>';
                  _.each(api_cols, function(col, ci){
                    out +=  '<td class="'+col+'">' + val[$.trim(col)] + '</td>';
                  });
          out +=  '<td class="source">'+apisource+'</th>';
          out +=  '</tr>';

        });

        out +=  '</tbody>'+
                  '</table>';

        $("#api_external_users .main").html( out );
      });
    }
});
