<?php
/**
 *
 */
class userStats
{
  public $dbtable = 'wiml_users';
  public $deactived;
  public $all;
  public $usedData;
  public $blacklist;
  public $deleted;

  function __construct()
  {
    global $wpdb;
    $this->deactived = "blacklist=0 AND active=1";
    $this->usedData = $wpdb->get_var("SELECT COUNT(*) FROM " . $this->dbtable . " WHERE ". $this->deactived);
    $this->blacklist = $wpdb->get_var("SELECT COUNT(*) FROM " . $this->dbtable ." WHERE blacklist=1");
    $this->deleted = $wpdb->get_var("SELECT COUNT(*) FROM " . $this->dbtable ." WHERE active=0 OR blacklist=1");
    $this->deleted = ($this->deleted - $this->blacklist);
    $this->all= ($this->usedData + $this->blacklist + $this->deleted);
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */
  public function display()
  {
    $args = array(
    "postbox_class" => array("first" => "", "second" => "", "third" => "")
    );

    $widgets = array(
    array("align" => "first",   "height"  =>  30, "title" => "Allgemein", "data" => $this->general() ),
    array("align" => "first",   "height"  =>  70, "title" => "Club (" . $this->deactived . ")", "data" => $this->club_active() ),
    array("align" => "second",  "height"  =>  40, "title" => "Neue Users im Jahr (" . current_time("Y") . ")", "data" => $this->thisYearActive() ),
    array("align" => "second",  "height"  =>  60, "title" => "Jährlich (herkunftsdatum | (" . $this->deactived . ")", "data" => $this->YearsBlacklist() ),
    array("align" => "third",   "height"  =>  100, "title" => "Blacklist Jährlich", "data" => $this->YearsActive() ),
    );

    $MSDashboard = new MSDashboard( $widgets, $args );
    echo $MSDashboard->display();
  }
  /*
    fn
  */
  public function general()
  {
    global $wpdb;
    $gesamt = 0;

    $out = '<table class="wp-list-table widefat striped">';
    $out .= '<tr><td class="highlight">Benutzte Daten</td><td class="highlight">'. $this->usedData  .'</td></tr>';
    $out .= '<tr><td>blacklist</td><td>'. $this->blacklist .'</td></tr>';
    $out .= '<tr><td>deteled</td><td>'. $this->deleted .'</td></tr>';
    $out .= '<tr><td>Alle</td><td>'. ($this->usedData + $this->blacklist + $this->deleted).'</td></tr>';
    $out .= '</table>';

    return $out;
  }
  /*
    fn
  */
  public function club_active()
  {
    global $wpdb;
    $gesamt   = 0;
    $percent  = 0;
    $gesamtPercent  = 0;
    $sql      = "SELECT COUNT(*) as anz, club FROM " . $this->dbtable ." WHERE ". $this->deactived ." GROUP BY club ORDER BY anz desc";
    $users    = $wpdb->get_results( $sql );

    $out      = '<table class="wp-list-table widefat striped">';

    foreach( $users as $user ) :
      $percent = round(($user->anz/$this->usedData*100));
      $gesamt += $user->anz;
      $gesamtPercent += $percent;

      $out .= '<tr>';
      $out .= '<td>'. $user->club .'</td>';
      $out .= '<td>'. $user->anz .'</td>';
      $out .= '<td>'. $percent .'%</td>';
      $out .= '</tr>';

    endforeach;

    $out    .= '<tr><td>Gesamt</td><td>'.$gesamt.'</td><td>'.$gesamtPercent.'%</td></tr>';
    $out    .= '</table>';

    return $out;
  }
  /*
    fn
  */
  public function thisYearActive()
  {
    global $wpdb;

    $gesamt = 0;
    $sql     = "SELECT COUNT(*) as anz, club FROM " . $this->dbtable ." WHERE ". $this->deactived ." AND YEAR(herkunftsdatum)='".current_time("Y")."' GROUP BY club ORDER BY anz desc";
    $users  = $wpdb->get_results( $sql );
    $out    = '<table class="wp-list-table widefat striped">';

    foreach( $users as $user ) :
      $out .= '<tr>';
      $out .= '<td>'. $user->club .'</td>';
      $out .= '<td>'. $user->anz .'</td>';

      $out .= '</tr>';
      $gesamt += $user->anz;

    endforeach;
    $out .= '<tr><td>Gesamt</td><td>'.$gesamt.'</td></tr>';
    $out .= '</table>';

    return $out;
  }
  /*
    fn
  */
  public function YearsActive()
  {
    global $wpdb;
    $gesamt = 0;
    $percent = 0;
    $gesamtPercent = 0;
    /*58184*/
    $sql = "SELECT COUNT(*) as anz, YEAR(herkunftsdatum) as hd FROM " . $this->dbtable ." WHERE YEAR(herkunftsdatum)!='0' AND ". $this->deactived ."  GROUP BY hd ORDER BY hd desc";
    /*var_dump($sql);*/
    $users = $wpdb->get_results( $sql );

    $args = array(
      "maxHeight" => 60,
      "unit"      => 100,
      "barWidth"  => 40,
      "x" => array("title" => "Monat", "col" => "hd"),
      "y" => array("title" => "Anzahl", "col" => "anz"),
      "data" => $users);

    $MSCharts = new MSCharts( $args );
    $out = $MSCharts->bar();

    $out .= '<table class="wp-list-table widefat striped">';
    foreach( $users as $user ) :

      $percent = round(($user->anz/$this->usedData*100));
      $out .= '<tr>';
      $out .= '<td>'. $user->hd .'</td>';
      $out .= '<td>'. $user->anz .'</td>';
      $out .= '<td>'. $percent .'%</td>';
      $out .= '</tr>';
      $gesamt += $user->anz;
      $gesamtPercent += $percent;

    endforeach;
    $out .= '<tr><td class="highlight">Gesamt</td><td class="highlight">'.$gesamt.'</td><td class="highlight">'.$gesamtPercent.'%</td></tr>';
    $out .= '<tr><td>Ohne Datum</td><td>'.($this->usedData-$gesamt).'</td><td>'.(100-$gesamtPercent).'%</td></tr>';
    $out .= '</table>';

    return $out;
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */
   public function YearsBlacklist()
   {
     global $wpdb;
     $gesamt = 0;
     $percent = 0;
     $gesamtPercent = 0;
     $sql = "SELECT COUNT(*) as anz, YEAR(blacklist_datum) as hd FROM " . $this->dbtable ." WHERE blacklist=1 GROUP BY hd ORDER BY hd desc";
     /*var_dump($sql);*/
     $users = $wpdb->get_results( $sql );
     $out   = '<table class="wp-list-table widefat striped">';
     foreach( $users as $user ) :
       $percent = round(($user->anz/$this->blacklist*100));
       $out .= '<tr>';
       $out .= '<td>'. $user->hd .'</td>';
       $out .= '<td>'. $user->anz .'</td>';
       $out .= '<td>'. $percent .'%</td>';
       $out .= '</tr>';
       $gesamt += $user->anz;
       $gesamtPercent += $percent;
     endforeach;
     $out .= '<tr><td>Gesamt</td><td>'.$gesamt.'</td><td>'.$gesamtPercent.'%</td></tr>';
     $out .= '</table>';

     return $out;
   }

}

?>
