<?php
/**
 *
 */
class usersImportByApi
{
  public $db_users_external = 'wiml_users_external';
  public $db_users          = 'wiml_users';

  function __construct(){
    global $wpdb;
  }

  public function display(){
    $args = array(
    "postbox_class" => array("first" => "col-4", "second" => "col-8")
    );

    $widgets = array(
    array("align" => "first",   "height"  =>  100, "title" => "Api", "id" => "users_api_manager", "data" => $this->api_structur() ),
    array("align" => "second",  "height"  =>  100, "title" => "external Users", "id" => "api_external_users"),
    );

    $MSDashboard = new MSDashboard( $widgets, $args );
    echo $MSDashboard->display();
  }
  /*
    fn
  */
  public static function api_structur(){
    global $wpdb;
    $gesamt = 0;
    $out =  '<div class="addnew"><a href="#" class="button">Neues API</a>  </div>';
    $out .= '<table class="wp-list-table widefat striped">';
    $out .= '</table>';

    return $out;
  }
  /*
    fn
  */
  public static function apis(){
    global $wpdb;
    $args = array( 'posts_per_page' => -1, 'post_type' => 'users_api' );
    $posts = get_posts( $args );
    $data = array();
    foreach( $posts as $post ){
      $api_cols  = get_post_meta( $post->ID, 'api_cols', true );
      $usercount = get_post_meta( $post->ID, 'usercount', true );
      $data[]    = array("ID" => $post->ID, "post_title" => $post->post_title, "post_content" => $post->post_content, "api_cols" => $api_cols, "usercount" => $usercount);
    }
    wp_send_json( $data );
    wp_die();
  }
  /*
    fn
  */
  public static function api_single(){
    global $wpdb;
    $post_id = isset( $_REQUEST["post_id"] ) ? $_REQUEST["post_id"] : '';

    if( !$post_id ) wp_die('Keine ID');

    $post = get_post( $post_id );
    $post->api_cols = get_post_meta( $post_id, 'api_cols', true );


    wp_send_json( $post );
    wp_die();
  }
  /*
    fn
  */
  public static function api_create(){
    global $wpdb;
    $post_type     = 'users_api';
    $post_title    = isset( $_REQUEST["api_title"] ) ? $_REQUEST["api_title"] : '';
    $post_content  = isset( $_REQUEST["api_url"] ) ? $_REQUEST["api_url"] : '';
    $post_id       = isset( $_REQUEST["api_post_id"] ) ? $_REQUEST["api_post_id"] : '';
    $post_api_cols = isset( $_REQUEST["api_cols"] ) ? $_REQUEST["api_cols"] : '';

    if( !$post_title || !$post_content ){
      wp_die("Kein Titel oder keine URL");
    }

    if( $post_id ){
      $usersImportByApi = new usersImportByApi();
      $usersImportByApi->api_update( array("ID" => $post_id, "post_title" => $post_title, "post_content" => $post_content, 'meta_input' => array('api_cols' => $post_api_cols)) );
      wp_die();
    }

    $arg = array(
      "post_type"    => $post_type,
      'post_status'  => 'publish',
      "post_title"   => $post_title,
      'post_content' => $post_content,
      'meta_input'   => array(
          'api_cols' => $post_api_cols
      )
    );
    wp_insert_post( $arg );
    wp_die();
  }
  /*
    fn
  */
  public static function api_update($args){
    global $wpdb;
    $args['post_status']  = 'publish';
    wp_update_post( $args );
    wp_send_json( $args );
  }
  /*
    fn
  */
  public static function api_update_usercount(){
    global $wpdb;
    $post_id       = isset( $_REQUEST["api_post_id"] ) ? $_REQUEST["api_post_id"] : '';
    $usercount     = isset( $_REQUEST["usercount"] ) ? $_REQUEST["usercount"] : '';

    if( $post_id ){
      update_post_meta( $post_id, 'usercount', $usercount );

      $args['ID']  = $post_id;
      $args['usercount']  = $usercount;
      wp_send_json( $args );
    }
  }
  /*
    fn
  */
  public static function api_delete(){
    global $wpdb;
  }
  /*
    fn
  */
  public static function getExtUsers(){
    global $wpdb;


    $source = isset( $_REQUEST["source"] ) ? $_REQUEST["source"] : "";
    if(!$source){
      wp_die( "KEINE QUELLE" );
    }
    $class  = new usersImportByApi();
    $sql    = "SELECT email FROM " . $class->db_users_external . " WHERE source='". $source ."' ORDER BY id DESC";
    $data   = $wpdb->get_results( $sql );

    wp_send_json( $data );
    wp_die();
  }
  /*
    fn
  */
  public static function import(){
    global $wpdb;
    $class    = new usersImportByApi();
    $db_users = $class->db_users;

    $email          = isset( $_REQUEST["email"] ) ? $_REQUEST["email"]                   : '';
    $confirm        = isset( $_REQUEST["confirm"] ) ? $_REQUEST["confirm"]               : 0;
    $username       = isset( $_REQUEST["username"] ) ? $_REQUEST["username"]             : '';
    $firstname      = isset( $_REQUEST["firstname"] ) ? $_REQUEST["firstname"]           : '';
    $lastname       = isset( $_REQUEST["lastname"] ) ? $_REQUEST["lastname"]             : '';
    $fullname       = isset( $_REQUEST["fullname"] ) ? $_REQUEST["fullname"]             : '';
    $herkunft       = isset( $_REQUEST["herkunft"] ) ? $_REQUEST["herkunft"]             : '';
    $herkunftsdatum = isset( $_REQUEST["herkunftsdatum"] ) ? $_REQUEST["herkunftsdatum"] : '';

    $arg = array(
      "email"          => $email,
      "confirm"        => $confirm,
      "username"       => $username,
      "firstname"      => ($firstname == "null") ? "" : $firstname,
      "lastname"       => ($lastname == "null") ? "" : $lastname,
      "fullname"       => $fullname,
      "herkunft"       => $herkunft,
      "herkunftsdatum" => $herkunftsdatum
    );


    $class->insertExternalUsers($arg);

    $sql_checkMail = "SELECT * FROM ".$db_users." WHERE email_address ='".$email."'";
    $row           = $wpdb->get_row($sql_checkMail);

    if ( is_object($row)  ){
      wp_send_json( array("status" => "exists") );
    } else {
      if ($confirm == 1) $class->user_insert($arg);
      wp_send_json( array("status" => "External User exists") );
    }

    wp_die();
  }
  /*
    fn
  */
  public static function insertExternalUsers( $arg ){
    global $wpdb;
    $result = FALSE;
    $class  = new usersImportByApi();
    $table  = $class->db_users_external;

    $data   = array(
      "username"			  => $arg["username"],
      "firstname"		    => $arg["firstname"],
      "lastname"		    => $arg["lastname"],
      "email"		        => $arg["email"],
      "status"		      => $arg["confirm"],
      "source"		      => $arg["herkunft"],
      "created"       	=> $arg["herkunftsdatum"],
    );
    $format = array('%s');

    $sql  = "SELECT * FROM ".$table." WHERE email='".$arg["email"]."' AND source='".$arg["herkunft"]."'";
    $row  = $wpdb->get_row($sql);

    if ( !is_object($row)  ) $wpdb->insert( $table, $data, $format );
  }
  /*
    fn
  */
  public static function user_insert( $arg ){
    global $wpdb;
    $result = FALSE;
    $class  = new usersImportByApi();
    $table  = $class->db_users;
    $data   = array(
      "confirm"			    => $arg["confirm"],
      "email_address"	  => $arg["email"],
      "firstname"		    => $arg["firstname"],
      "lastname"		    => $arg["lastname"],
      "fullname"		    => $arg["fullname"],
      "herkunft"		    => $arg["herkunft"],
      "herkunftsdatum"	=> $arg["herkunftsdatum"],
      "club"			      => $arg["herkunft"],
      "importdatum"		  => current_time("Y-m-d H:i:s"),
      "updatetime"		  => current_time("Y-m-d H:i:s")
    );
    $format = array('%s');
    $result = $wpdb->insert( $table, $data, $format );
    if( $result ){
      wp_send_json( array("status" => "insert") );
    }else{
      wp_send_json( array("status" => "no") );
    }
    wp_die();
  }
  /*
    fn
  */
  public static function create_dbTable(){
    global $wpdb, $db_wiml_users_external;

    $db_wiml_users_external = '1.0';
    $installed_ver          = get_option( "db_wiml_users_external" );
    $class                   = new usersImportByApi();

    if ( $db_wiml_users_external != $installed_ver ) {

        $charset_collate = $wpdb->get_charset_collate();
        $table_name  = $class->db_users_external;

        $sql = "CREATE TABLE $table_name (
          id bigint(20) NOT NULL AUTO_INCREMENT,
          username varchar(30) DEFAULT '' NOT NULL,
          firstname varchar(30) DEFAULT '' NOT NULL,
          lastname varchar(30) DEFAULT '' NOT NULL,
          email varchar(55) DEFAULT '' NOT NULL,
          status tinyint(1) DEFAULT 0 NOT NULL,
          source varchar(255) DEFAULT '' NOT NULL,
          created datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
          PRIMARY KEY  (id)
        ) $charset_collate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );

        add_option( 'db_wiml_users_external', $db_wiml_users_external );
      }
  }
}
